from django.shortcuts import render
from datetime import *




# Enter your name here
mhs_name = 'Ibnu Sofian Firdaus' # TODO Implement this
mhs_born = 1998
# Create your views here.
def index(request):
    response = {'name': mhs_name,'age' : calculate_age(mhs_born)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
	currentYear = datetime.now().year
	currentAge = currentYear - birth_year
	return currentAge
